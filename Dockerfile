
FROM alpine:3

RUN apk add --update --no-cache openssh ansible
RUN ansible-galaxy collection install community.docker